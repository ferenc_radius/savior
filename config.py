paths = dict(
    photos='/mnt/photos',
)

options = dict(
    destination="/tmp/",
    userKnownHostsFile="/home/john-doe/.ssh/known_hosts",
    privateKey="/home/john-doe/.ssh/id_rsa",
    host="servername",
    user="john-doe",
    port=22,
    sync=60*60*24
)