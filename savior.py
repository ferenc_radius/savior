#!/usr/bin/env python
import config
from host import Host
from log import Log

host = Host(config.options.get("host"), config.options.get("port"), config.options.get("usr"))
log = Log("savior.db")
lastSync = log.lastSynced(config.options.get("host"))

if lastSync < config.options.get("sync"):
    print "already synced!"
    exit(1)

if host.connect():
    host.syncAll(config.paths)
    log.addEntry(config.options.get("host"))

else:
    print "no reponse from server, we are done"

