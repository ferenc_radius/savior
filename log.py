import sqlite3
import os
import time
import datetime


class Log():

    def __init__(self, dbFile):
        self.file = dbFile

        if not os.path.isfile(dbFile):
            self.createFile()

        self.connect()

    def connect(self):
        self.conn = sqlite3.connect(self.file)

    def createFile(self):

        self.connect()

        c = self.conn.cursor()
        c.execute("CREATE TABLE hosts (date INT, hostname text)")

        self.conn.commit()
        self.conn.close()

    def addEntry(self, hostname):
        c = self.conn.cursor()

        future = datetime.datetime.now()
        c.execute("INSERT INTO hosts VALUES (?, ?)", [time.mktime(future.timetuple()), hostname])

        # Save (commit) the changes
        self.conn.commit()

    def lastSynced(self, hostname):
        self.conn = sqlite3.connect('savior.db')

        c = self.conn.cursor()
        c.execute("SELECT date,hostname FROM hosts where hostname = ? order by date desc", [hostname])
        record = c.fetchone()

        if record is not None:
            syncTime = record[0]

            dt = datetime.datetime.fromtimestamp(syncTime)
            left = datetime.datetime.now() - dt

            return left.total_seconds()

        return 0

    def __del__(self):
       if self.conn is not None:
           self.conn.close()


