import socket
import string
import subprocess
import paramiko
import config
import sqlite3
import os

class Host():

    def __init__(self, host, port, user):
        self.client = None
        self.host = host
        self.port = port
        self.user = user

    def connect(self):

        client = paramiko.SSHClient()
        client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        client.load_system_host_keys()

        try:
            #we use a public key authentication :)
            client.connect(self.host, port=self.port,
                                username=self.user)
            stdin, stdout, stderr = client.exec_command("uptime")

            print stdout.readlines()

            return True

        except (paramiko.SSHException, socket.error):
            return False

    def syncAll(self, paths):
        for pathName in paths:
            self.__syncDir(paths[pathName])

    def __syncDir(self, path):
        destination = config.options.get("destination")
        userKnownHostsFile = config.options.get("userKnownHostsFile")
        privateKey = config.options.get("privateKey")

        command = [
            "rsync",
            "-rztv",
            "--delete",
            "--stats",
            "--progress",
            "--dry-run",
            "-e",
            "ssh -p " + self.port + " -o BatchMode=yes -o UserKnownHostsFile='" +
            userKnownHostsFile + "' -i '" + privateKey + "'",
            self.user + "@" + self.host + ":" + path,
            destination
        ]

        print string.join(command, " ")
        sp = subprocess.call(command)
